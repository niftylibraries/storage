<?php

    /**
     * Storage access interface
     *
     * @category   Nifty
     * @package    Storage
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    0.1.1 Build 06041
     * @since      0.1
     */

    namespace Nifty\Storage\Access;

    /**
     * StorageAccessInterface interface.
     */
    interface StorageAccessInterface {}

    /**
     * StorageAccessException class.
     */
    class StorageAccessException extends \Exception {}