<?php

    /**
     * Interface for storage access collection
     *
     * @category   Nifty
     * @package    Storage
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    0.1.1 Build 06041
     * @since      0.1 Build 2049
     */

    namespace Nifty\Storage\Access\Collections;

    /**
     * StorageAccessCollectionInterface interface.
     */
    interface StorageAccessCollectionInterface {

        /**
         * Returns all storage access instances.
         *
         * @access public
         * @return void
         */
        public function getInstances();

    }

    /**
     * StorageAccessCollectionException class.
     */
    class StorageAccessCollectionException extends \Exception {}