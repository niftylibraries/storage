<?php

    /**
     * Storage adapter interface
     *
     * @category   Nifty
     * @package    Storage
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    0.1.1 Build 06041
     * @since      0.1
     */

    namespace Nifty\Storage\Adapter;

    /**
     * StorageAdapterInterface interface.
     */
    interface StorageAdapterInterface {}

    /**
     * StorageAdapterException class.
     */
    class StorageAdapterException extends \Exception {}